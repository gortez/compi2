/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "tinyc.y" /* yacc.c:339  */

#include <cstdio>
#include <cstdlib>
#include <string>
#include "ast.h"

using namespace std;

extern int line;

int yylex();

void yyerror(const char *str)
{
    printf("Line %d: %s\n", line, str);
    exit(0);
}

#define YYERROR_VERBOSE 1

Class *input;


#line 90 "tinyc_parser.cpp" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "tokens.h".  */
#ifndef YY_YY_TOKENS_H_INCLUDED
# define YY_YY_TOKENS_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    NUM = 258,
    ID = 259,
    STR_CONST = 260,
    CHAR_L = 261,
    PRINT = 262,
    INTEGER = 263,
    DOUBLE = 264,
    OP_LT = 265,
    OP_GT = 266,
    OP_LTE = 267,
    OP_GTE = 268,
    OP_NE = 269,
    OP_EQ = 270,
    KW_AND = 271,
    KW_OR = 272,
    KW_SHIFT_LEFT = 273,
    KW_SHIFT_RIGHT = 274,
    KW_BOOL = 275,
    KW_BREAK = 276,
    KW_PRINT = 277,
    KW_READ = 278,
    KW_CONTINUE = 279,
    KW_CLASS = 280,
    KW_ELSE = 281,
    KW_EXTENDS = 282,
    KW_FALSE = 283,
    KW_FOR = 284,
    KW_IF = 285,
    KW_INT = 286,
    KW_REAL = 287,
    KW_NEW = 288,
    KW_NULL = 289,
    KW_RETURN = 290,
    KW_ROT = 291,
    KW_TRUE = 292,
    KW_VOID = 293,
    KW_WHILE = 294,
    KW_CONST_STRING = 295,
    U_MINUS = 296
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 25 "tinyc.y" /* yacc.c:355  */

    char *id_t;
    int  num_t;
    Statement *statement_t;
    list<Statement*> * statement_list_t;
    ExprList *exprlist_t;
    Expr *expr_single;
    IdExpr * id_expr_t;
    IdDeclare *id_declare_t;
    VariablesDeclareStatement * var_declar_single_t;
    list<VariablesDeclareStatement*> * list_var_declr_t;
    list<MethodDeclareStatement *> * list_method_declr_t;
    MethodDeclareStatement * method_single_t;
    DataType data_type_t;
    BlockVariableStatement * block_var_single_t;
    list<IdElement*> * id_list_t;
    list<ArgumentElement *> * arg_list_t;
    list<BlockVariableStatement *> * local_list_declr_t;
    list<string> * str_list_t;


#line 194 "tinyc_parser.cpp" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_TOKENS_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 211 "tinyc_parser.cpp" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   328

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  57
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  25
/* YYNRULES -- Number of rules.  */
#define YYNRULES  76
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  147

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   296

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    46,     2,     2,     2,    41,     2,     2,
      55,    56,    44,    42,    54,    43,     2,    45,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    50,
       2,    51,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    52,     2,    53,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    48,     2,    49,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    47
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    90,    90,    96,    97,   101,   104,   111,   116,   122,
     127,   132,   134,   137,   142,   146,   149,   156,   159,   165,
     168,   171,   174,   178,   180,   181,   186,   191,   192,   194,
     195,   198,   199,   200,   201,   202,   203,   204,   205,   207,
     208,   210,   211,   217,   218,   221,   222,   224,   225,   226,
     227,   229,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   253,   255,   261
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NUM", "ID", "STR_CONST", "CHAR_L",
  "PRINT", "INTEGER", "DOUBLE", "OP_LT", "OP_GT", "OP_LTE", "OP_GTE",
  "OP_NE", "OP_EQ", "KW_AND", "KW_OR", "KW_SHIFT_LEFT", "KW_SHIFT_RIGHT",
  "KW_BOOL", "KW_BREAK", "KW_PRINT", "KW_READ", "KW_CONTINUE", "KW_CLASS",
  "KW_ELSE", "KW_EXTENDS", "KW_FALSE", "KW_FOR", "KW_IF", "KW_INT",
  "KW_REAL", "KW_NEW", "KW_NULL", "KW_RETURN", "KW_ROT", "KW_TRUE",
  "KW_VOID", "KW_WHILE", "KW_CONST_STRING", "'%'", "'+'", "'-'", "'*'",
  "'/'", "'!'", "U_MINUS", "'{'", "'}'", "';'", "'='", "'['", "']'", "','",
  "'('", "')'", "$accept", "input", "global_declare_various",
  "global_declare_one", "id_list", "id_individual", "method_various",
  "method_individual", "ListaArgumentos", "ArgumentProd", "type", "block",
  "block_var_various", "block_var_individual", "variableList",
  "block_statements", "statement_single", "methodParameter", "parameter",
  "else_block", "return_expr", "constant", "expr", "MethodExpr",
  "arr_or_id", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,    37,    43,    45,    42,    47,    33,   296,   123,   125,
      59,    61,    91,    93,    44,    40,    41
};
# endif

#define YYPACT_NINF -63

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-63)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -18,    25,    30,   -17,   -63,   -63,    -6,   -63,   -63,   -63,
     -63,   -15,   -16,    -6,    50,   -63,   -63,   -63,    51,    33,
     -63,     1,    -1,    55,    56,    63,   -63,    13,   -63,   -63,
     -63,   -63,   -63,    15,   -44,   -63,    65,    -6,   -63,    28,
      67,   -63,    32,    86,   -63,    41,   -63,    39,    -6,    40,
      43,   123,    -6,    28,    95,   -63,    96,   -63,   -49,    52,
      16,    74,    70,    16,   -63,   -63,   -63,    76,    77,   -63,
     -63,    32,    43,    16,    16,   -63,   -63,   -63,    16,    16,
      16,    79,   149,   -63,   -63,   -63,    16,    80,   217,   -63,
      16,   -63,   -63,   163,    75,   -63,   -63,    62,   -63,    16,
      16,    16,    16,    16,    16,    16,    16,    16,    16,    16,
      16,    16,    16,    16,    16,    16,   -63,    98,   -63,   207,
     -63,   -63,   -63,   283,   283,   283,   283,   273,   273,   263,
     253,    22,    22,    22,    78,   -32,   -32,   -63,   -63,   149,
      13,   -63,   -63,   106,    13,   -63,   -63
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     0,     1,     3,    12,    19,    20,    21,
      22,     0,     0,    12,     0,     4,     2,    13,     0,    11,
       5,     0,     0,     0,     0,     0,     7,     0,    47,    50,
      49,    48,     6,     0,    11,    15,     0,    24,    14,    11,
       0,     9,    17,     0,    29,     0,     8,     0,     0,     0,
      27,     0,    24,    11,     0,    16,     0,    26,    75,     0,
      39,     0,     0,    45,    23,    38,    30,     0,     0,    25,
      10,    17,    27,     0,    39,    33,    70,    71,     0,     0,
       0,     0,    41,    73,    72,    35,     0,     0,    46,    32,
       0,    18,    28,     0,     0,    69,    67,     0,    37,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    40,     0,    34,     0,
      76,    74,    68,    57,    60,    58,    59,    55,    56,    54,
      53,    52,    51,    61,    62,    64,    63,    66,    65,    41,
       0,    31,    42,    43,     0,    36,    44
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -63,   -63,   -63,   -63,   -63,   -30,   120,   -63,   -63,    64,
       9,   -51,    84,   -63,    66,   -63,   -63,    72,    -2,   -63,
     -63,   -63,   -62,    97,    99
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     6,    11,    20,    26,    12,    13,    27,    49,
      43,    38,    44,    45,    57,    51,    66,    81,   116,   145,
      87,    32,    82,    83,    84
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      65,    88,    28,    73,    41,    29,    74,     1,    40,    46,
      24,    93,   113,   114,     7,    14,    95,    96,    97,    76,
      58,    77,    18,    70,   117,     8,     9,    30,   119,     3,
       4,     5,    10,    16,    36,    15,    31,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,    19,    21,    25,    54,    33,    78,
      34,    37,    79,   110,   111,   112,   113,   114,    39,    42,
      47,    80,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,    24,     7,    22,    23,    48,    24,    25,   143,
      50,    52,    53,   146,     8,     9,    55,    56,   109,    71,
      72,    10,    75,   110,   111,   112,   113,   114,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   122,    35,
     111,   112,   113,   114,    85,    86,    89,    58,    90,    98,
     118,   121,   144,    17,   109,    91,    69,   142,    92,   110,
     111,   112,   113,   114,    59,    60,    94,    61,    67,     0,
      68,     0,     0,    62,   140,     0,     0,     0,    63,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,     0,
       0,    37,    64,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,     0,     0,   109,     0,     0,     0,     0,
     110,   111,   112,   113,   114,     0,     0,     0,     0,   109,
       0,     0,     0,   115,   110,   111,   112,   113,   114,     0,
       0,     0,     0,     0,     0,     0,   120,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,     0,     0,     0,
       0,     0,     0,   109,     0,     0,     0,     0,   110,   111,
     112,   113,   114,   109,     0,     0,     0,   141,   110,   111,
     112,   113,   114,    99,   100,   101,   102,   103,   104,   105,
       0,   107,   108,    99,   100,   101,   102,   103,   104,     0,
       0,   107,   108,    99,   100,   101,   102,     0,     0,   109,
       0,   107,   108,     0,   110,   111,   112,   113,   114,   109,
       0,   107,   108,     0,   110,   111,   112,   113,   114,   109,
       0,     0,     0,     0,   110,   111,   112,   113,   114,   109,
       0,     0,     0,     0,   110,   111,   112,   113,   114
};

static const yytype_int16 yycheck[] =
{
      51,    63,     3,    52,    34,     6,    55,    25,    52,    39,
      54,    73,    44,    45,    20,     6,    78,    79,    80,     3,
       4,     5,    13,    53,    86,    31,    32,    28,    90,     4,
       0,    48,    38,    49,    25,    50,    37,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,     4,     4,    55,    48,     3,    43,
       4,    48,    46,    41,    42,    43,    44,    45,    53,     4,
       3,    55,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    54,    20,    51,    52,    54,    54,    55,   140,
       4,    50,    53,   144,    31,    32,    56,    54,    36,     4,
       4,    38,    50,    41,    42,    43,    44,    45,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    56,    56,
      42,    43,    44,    45,    50,    55,    50,     4,    51,    50,
      50,    56,    26,    13,    36,    71,    52,   139,    72,    41,
      42,    43,    44,    45,    21,    22,    74,    24,    51,    -1,
      51,    -1,    -1,    30,    56,    -1,    -1,    -1,    35,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    -1,
      -1,    48,    49,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    -1,    -1,    36,    -1,    -1,    -1,    -1,
      41,    42,    43,    44,    45,    -1,    -1,    -1,    -1,    36,
      -1,    -1,    -1,    54,    41,    42,    43,    44,    45,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    53,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    -1,    -1,    -1,
      -1,    -1,    -1,    36,    -1,    -1,    -1,    -1,    41,    42,
      43,    44,    45,    36,    -1,    -1,    -1,    50,    41,    42,
      43,    44,    45,    10,    11,    12,    13,    14,    15,    16,
      -1,    18,    19,    10,    11,    12,    13,    14,    15,    -1,
      -1,    18,    19,    10,    11,    12,    13,    -1,    -1,    36,
      -1,    18,    19,    -1,    41,    42,    43,    44,    45,    36,
      -1,    18,    19,    -1,    41,    42,    43,    44,    45,    36,
      -1,    -1,    -1,    -1,    41,    42,    43,    44,    45,    36,
      -1,    -1,    -1,    -1,    41,    42,    43,    44,    45
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    25,    58,     4,     0,    48,    59,    20,    31,    32,
      38,    60,    63,    64,    67,    50,    49,    63,    67,     4,
      61,     4,    51,    52,    54,    55,    62,    65,     3,     6,
      28,    37,    78,     3,     4,    56,    67,    48,    68,    53,
      52,    62,     4,    67,    69,    70,    62,     3,    54,    66,
       4,    72,    50,    53,    67,    56,    54,    71,     4,    21,
      22,    24,    30,    35,    49,    68,    73,    80,    81,    69,
      62,     4,     4,    52,    55,    50,     3,     5,    43,    46,
      55,    74,    79,    80,    81,    50,    55,    77,    79,    50,
      51,    66,    71,    79,    74,    79,    79,    79,    50,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    36,
      41,    42,    43,    44,    45,    54,    75,    79,    50,    79,
      53,    56,    56,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      56,    50,    75,    68,    26,    76,    68
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    57,    58,    59,    59,    60,    60,    61,    61,    62,
      62,    62,    63,    63,    64,    65,    65,    66,    66,    67,
      67,    67,    67,    68,    69,    69,    70,    71,    71,    72,
      72,    73,    73,    73,    73,    73,    73,    73,    73,    74,
      74,    75,    75,    76,    76,    77,    77,    78,    78,    78,
      78,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    80,    81,    81
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     6,     0,     3,     2,     4,     2,     5,     3,
       6,     0,     0,     2,     4,     2,     5,     0,     4,     1,
       1,     1,     1,     4,     0,     3,     3,     0,     3,     0,
       2,     4,     2,     2,     3,     2,     6,     3,     1,     0,
       2,     0,     3,     0,     2,     0,     1,     1,     1,     1,
       1,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     2,     3,     2,
       1,     1,     1,     1,     4,     1,     4
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 93 "tinyc.y" /* yacc.c:1661  */
    { input = new Class((yyvsp[-2].list_var_declr_t),(yyvsp[-1].list_method_declr_t)); }
#line 1429 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 3:
#line 96 "tinyc.y" /* yacc.c:1661  */
    { (yyval.list_var_declr_t) = new list<VariablesDeclareStatement*>();}
#line 1435 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 4:
#line 97 "tinyc.y" /* yacc.c:1661  */
    { (yyval.list_var_declr_t) = (yyvsp[-2].list_var_declr_t);
                                                                        (yyval.list_var_declr_t)->push_back((yyvsp[-1].var_declar_single_t));
                                                                      }
#line 1443 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 5:
#line 101 "tinyc.y" /* yacc.c:1661  */
    { 
                                                 (yyval.var_declar_single_t) = new VariablesDeclareStatement((yyvsp[-1].data_type_t),(yyvsp[0].id_list_t),new NumExpr(0));
                                             }
#line 1451 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 6:
#line 104 "tinyc.y" /* yacc.c:1661  */
    {
                                                list<IdElement*> * _list = new list<IdElement*>();
                                                IdElementVar *id_element=new IdElementVar((yyvsp[-2].id_t));
                                                _list->push_back(id_element);
                                                (yyval.var_declar_single_t) = new VariablesDeclareStatement((yyvsp[-3].data_type_t), _list, (yyvsp[0].expr_single));
                                             }
#line 1462 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 7:
#line 111 "tinyc.y" /* yacc.c:1661  */
    {
                                        IdElementVar *id_element=new IdElementVar((yyvsp[-1].id_t));
                                        (yyvsp[0].id_list_t)->push_front(id_element);
                                        (yyval.id_list_t)=(yyvsp[0].id_list_t);
                                      }
#line 1472 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 8:
#line 116 "tinyc.y" /* yacc.c:1661  */
    {
                                        IdElementArr *id_element=new IdElementArr((yyvsp[-4].id_t),(yyvsp[-2].num_t));
                                        (yyvsp[0].id_list_t)->push_front(id_element);
                                        (yyval.id_list_t)=(yyvsp[0].id_list_t);
                                      }
#line 1482 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 9:
#line 122 "tinyc.y" /* yacc.c:1661  */
    {
                                                    (yyval.id_list_t) =(yyvsp[0].id_list_t);
                                                    IdElementVar *id_element=new IdElementVar((yyvsp[-1].id_t));
                                                    (yyval.id_list_t)->push_front(id_element); 
                                                }
#line 1492 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 10:
#line 127 "tinyc.y" /* yacc.c:1661  */
    {
                                                    (yyval.id_list_t)=(yyvsp[0].id_list_t);
                                                    IdElementArr *id_element=new IdElementArr((yyvsp[-4].id_t),(yyvsp[-2].num_t));
                                                    (yyval.id_list_t)->push_front(id_element);
                                                }
#line 1502 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 11:
#line 132 "tinyc.y" /* yacc.c:1661  */
    {   (yyval.id_list_t) = new list<IdElement*>();    }
#line 1508 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 12:
#line 134 "tinyc.y" /* yacc.c:1661  */
    {
                                                    (yyval.list_method_declr_t) = new list<MethodDeclareStatement *>();
                                                }
#line 1516 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 13:
#line 137 "tinyc.y" /* yacc.c:1661  */
    {
                                                    (yyval.list_method_declr_t)=(yyvsp[0].list_method_declr_t); 
                                                    (yyval.list_method_declr_t)->push_front((yyvsp[-1].method_single_t));
                                                }
#line 1525 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 14:
#line 142 "tinyc.y" /* yacc.c:1661  */
    {
                                                    (yyval.method_single_t) = new MethodDeclareStatement((yyvsp[-3].data_type_t),(yyvsp[-2].id_t),(yyvsp[-1].arg_list_t),(yyvsp[0].statement_t));
                                                 }
#line 1533 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 15:
#line 146 "tinyc.y" /* yacc.c:1661  */
    {
                                    (yyval.arg_list_t) = new list<ArgumentElement *> ();
                                  }
#line 1541 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 16:
#line 149 "tinyc.y" /* yacc.c:1661  */
    {
                                    ArgumentElement *arg_element= new ArgumentElement((yyvsp[-3].data_type_t),(yyvsp[-2].id_t));
                                    (yyval.arg_list_t)=(yyvsp[-1].arg_list_t);
                                    (yyval.arg_list_t)->push_front(arg_element); 
                                  }
#line 1551 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 17:
#line 156 "tinyc.y" /* yacc.c:1661  */
    {
                                    (yyval.arg_list_t) = new list<ArgumentElement *>(); 
                                }
#line 1559 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 18:
#line 159 "tinyc.y" /* yacc.c:1661  */
    {
                                    (yyval.arg_list_t)=(yyvsp[0].arg_list_t);
                                    ArgumentElement *arg_element = new ArgumentElement((yyvsp[-2].data_type_t),(yyvsp[-1].id_t));
                                    (yyval.arg_list_t)->push_front(arg_element);
                                }
#line 1569 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 19:
#line 165 "tinyc.y" /* yacc.c:1661  */
    {
                (yyval.data_type_t) = DT_Bool;
              }
#line 1577 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 20:
#line 168 "tinyc.y" /* yacc.c:1661  */
    {
                (yyval.data_type_t) = DT_Int;
             }
#line 1585 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 21:
#line 171 "tinyc.y" /* yacc.c:1661  */
    {
                (yyval.data_type_t) = DT_Real;
             }
#line 1593 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 22:
#line 174 "tinyc.y" /* yacc.c:1661  */
    {
                (yyval.data_type_t) = DT_Void;
             }
#line 1601 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 23:
#line 178 "tinyc.y" /* yacc.c:1661  */
    { (yyval.statement_t)=new BlockStatement((yyvsp[-2].local_list_declr_t),(yyvsp[-1].statement_list_t)); }
#line 1607 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 24:
#line 180 "tinyc.y" /* yacc.c:1661  */
    {(yyval.local_list_declr_t) = new list<BlockVariableStatement *>();}
#line 1613 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 25:
#line 181 "tinyc.y" /* yacc.c:1661  */
    {
                                                                (yyval.local_list_declr_t) = (yyvsp[0].local_list_declr_t);
                                                                (yyvsp[0].local_list_declr_t)->push_front((yyvsp[-2].block_var_single_t));
                                                              }
#line 1622 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 26:
#line 186 "tinyc.y" /* yacc.c:1661  */
    {
                                                (yyvsp[0].str_list_t)->push_back((yyvsp[-1].id_t));
                                                (yyval.block_var_single_t)= new BlockVariableStatement((yyvsp[-2].data_type_t),(yyvsp[0].str_list_t));
                                           }
#line 1631 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 27:
#line 191 "tinyc.y" /* yacc.c:1661  */
    {(yyval.str_list_t) = new list<string>();}
#line 1637 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 28:
#line 192 "tinyc.y" /* yacc.c:1661  */
    {(yyval.str_list_t)=(yyvsp[0].str_list_t); (yyval.str_list_t)->push_front((yyvsp[-1].id_t)); }
#line 1643 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 29:
#line 194 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_list_t) = new list<Statement *>();}
#line 1649 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 30:
#line 195 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_list_t) = (yyvsp[-1].statement_list_t);(yyvsp[-1].statement_list_t)->push_back((yyvsp[0].statement_t));}
#line 1655 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 31:
#line 198 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = new AssigmentStament((yyvsp[-3].id_expr_t),(yyvsp[-1].expr_single));}
#line 1661 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 32:
#line 199 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = new MethodCallStatement((yyvsp[-1].expr_single)); }
#line 1667 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 33:
#line 200 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = new BreakStatement();}
#line 1673 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 34:
#line 201 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = new ReturnStatement((yyvsp[-1].expr_single));}
#line 1679 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 35:
#line 202 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = new ContinueStatement();}
#line 1685 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 36:
#line 203 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = new IfStatement((yyvsp[-3].expr_single),(yyvsp[-1].statement_t),(yyvsp[0].statement_t));}
#line 1691 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 37:
#line 204 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = new PrintStatement((yyvsp[-1].exprlist_t)); }
#line 1697 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 38:
#line 205 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = (yyvsp[0].statement_t);}
#line 1703 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 39:
#line 207 "tinyc.y" /* yacc.c:1661  */
    {(yyval.exprlist_t) = new list<Expr*>();}
#line 1709 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 40:
#line 208 "tinyc.y" /* yacc.c:1661  */
    {(yyval.exprlist_t)=(yyvsp[0].exprlist_t);(yyval.exprlist_t)->push_front((yyvsp[-1].expr_single));}
#line 1715 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 41:
#line 210 "tinyc.y" /* yacc.c:1661  */
    {(yyval.exprlist_t) = new list<Expr*>();}
#line 1721 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 42:
#line 211 "tinyc.y" /* yacc.c:1661  */
    {
                              (yyval.exprlist_t)=(yyvsp[0].exprlist_t); 
                              (yyval.exprlist_t)->push_front((yyvsp[-1].expr_single));
                             }
#line 1730 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 43:
#line 217 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = new BlockStatement(new list<BlockVariableStatement *>(), new list<Statement *>());}
#line 1736 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 44:
#line 218 "tinyc.y" /* yacc.c:1661  */
    {(yyval.statement_t) = (yyvsp[0].statement_t);}
#line 1742 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 45:
#line 221 "tinyc.y" /* yacc.c:1661  */
    {(yyval.expr_single) = new NumExpr(0);}
#line 1748 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 46:
#line 222 "tinyc.y" /* yacc.c:1661  */
    {(yyval.expr_single) = (yyvsp[0].expr_single);}
#line 1754 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 47:
#line 224 "tinyc.y" /* yacc.c:1661  */
    {  (yyval.expr_single) = new NumExpr((yyvsp[0].num_t)); }
#line 1760 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 48:
#line 225 "tinyc.y" /* yacc.c:1661  */
    {  (yyval.expr_single) = new BoolExpr(true); }
#line 1766 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 49:
#line 226 "tinyc.y" /* yacc.c:1661  */
    {  (yyval.expr_single) = new BoolExpr(false); }
#line 1772 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 50:
#line 227 "tinyc.y" /* yacc.c:1661  */
    {  (yyval.expr_single) = new CharExpr((yyvsp[0].num_t)); }
#line 1778 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 51:
#line 229 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new ShiftRightExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1784 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 52:
#line 230 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new ShiftLeftExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1790 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 53:
#line 231 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new OrExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1796 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 54:
#line 232 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new AndExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1802 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 55:
#line 233 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new NEExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1808 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 56:
#line 234 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new EQExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1814 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 57:
#line 235 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new LTExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1820 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 58:
#line 236 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new LTEExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1826 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 59:
#line 237 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new GTEExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1832 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 60:
#line 238 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new GTExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1838 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 61:
#line 239 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new RotExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1844 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 62:
#line 240 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new ModExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1850 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 63:
#line 241 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new SubExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1856 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 64:
#line 242 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new AddExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1862 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 65:
#line 243 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new DivExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1868 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 66:
#line 244 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new MultExpr((yyvsp[-2].expr_single),(yyvsp[0].expr_single)); }
#line 1874 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 67:
#line 245 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new NegExpr((yyvsp[0].expr_single));}
#line 1880 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 68:
#line 246 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = (yyvsp[-1].expr_single); }
#line 1886 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 69:
#line 247 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new MultExpr(new NumExpr(-1), (yyvsp[0].expr_single)); }
#line 1892 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 70:
#line 248 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new NumExpr((yyvsp[0].num_t)); }
#line 1898 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 71:
#line 249 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = new StringExpr((yyvsp[0].id_t)); }
#line 1904 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 72:
#line 250 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = (yyvsp[0].id_expr_t); }
#line 1910 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 73:
#line 251 "tinyc.y" /* yacc.c:1661  */
    { (yyval.expr_single) = (yyvsp[0].expr_single); }
#line 1916 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 74:
#line 253 "tinyc.y" /* yacc.c:1661  */
    {(yyval.expr_single) = new MethodCallExpr((yyvsp[-3].id_t),(yyvsp[-1].exprlist_t)); }
#line 1922 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 75:
#line 255 "tinyc.y" /* yacc.c:1661  */
    { 
                                string id = (yyvsp[0].id_t);
                                
                                free((yyvsp[0].id_t));
                                (yyval.id_expr_t) = new IdExpr(id);
                            }
#line 1933 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;

  case 76:
#line 261 "tinyc.y" /* yacc.c:1661  */
    {(yyval.id_expr_t) = new IdExpr((yyvsp[-3].id_t),(yyvsp[-1].expr_single));}
#line 1939 "tinyc_parser.cpp" /* yacc.c:1661  */
    break;


#line 1943 "tinyc_parser.cpp" /* yacc.c:1661  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 263 "tinyc.y" /* yacc.c:1906  */
