#ifndef _AST_H_ 
#define _AST_H_

#include <string>
#include <list>
#include <map>

using namespace std;

enum DataType {DT_Int, DT_Bool, DT_Void};

struct VValue {
    bool boolValue() {
        return ivalue>0;
    }
    
    int intValue() {
        return ivalue;
    }
    
    DataType type;
    string offset;
    
    int ivalue;
};

struct IdDeclare{
    string name;
    int length_array;
};

extern map<string, VValue> vars;

enum ExprKind {
  SR_EXPR,
  SL_EXPR,
  AND_EXPR,
  OR_EXPR,
  LT_EXPR,
  LTE_EXPR,
  GT_EXPR,
  GTE_EXPR,
  NE_EXPR,
  EQ_EXPR,
  ADD_EXPR,
  SUB_EXPR,
  MULT_EXPR,
  DIV_EXPR,
  NUM_EXPR,
  BOOL_EXPR,
  ID_EXPR,
  ROT_EXPR,
  MOD_EXPR,
  NEG_EXPR,
  CHAR_EXPR,
  METHOD_EXPR
};

enum StatementKind {
    BLOCK_STATEMENT,
    PRINT_STATEMENT,
    ASSIGN_STATEMENT,
    IF_STATEMENT,
    VARIABLE_DECLARE_STATEMENT,
    METHOD_DECLARE_STATEMENT,
    VARIABLE_BLOCK_STATEMENT,
    ASSIGMENT_STATEMENT,
    METHOD_CALL_STATEMENT,
    BREAK_STATEMENT,
    RETURN_STATEMENT,
    CONTINUE_STATEMENT
};

class Expr {
public:
    virtual VValue evaluate() = 0;
    virtual string generateCode(string &place) = 0;
    virtual int getKind() = 0;
    bool isA(int kind) { return (getKind() == kind); }
};

typedef list<Expr*> ExprList;
typedef list<IdDeclare*> IdList;

class BinaryExpr: public Expr {
public:
    BinaryExpr(Expr *expr1, Expr *expr2) {
        this->expr1 = expr1;
        this->expr2 = expr2;
    }
    
    Expr *expr1;
    Expr *expr2;
};

class NegExpr: public Expr {
public:
    NegExpr(Expr *expr1) {
        this->expr1 = expr1;
    }
    
    VValue evaluate(){}
    string generateCode(string &place);
    Expr *expr1;
    int getKind() { return NEG_EXPR; }
};

class ModExpr: public BinaryExpr {
public:
    ModExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);

    int getKind() { return MOD_EXPR; }
};

class RotExpr: public BinaryExpr {
public:
    RotExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate(){}
    string generateCode(string &place);
    int getKind() { return ROT_EXPR; }
};

class ShiftRightExpr: public BinaryExpr {
public:
    ShiftRightExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);

    int getKind() { return SR_EXPR; }
};

class ShiftLeftExpr: public BinaryExpr {
public:
    ShiftLeftExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return SR_EXPR; }
};

class AndExpr: public BinaryExpr {
public:
    AndExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return AND_EXPR; }
};

class OrExpr: public BinaryExpr {
public:
    OrExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return OR_EXPR; }
};

class LTExpr: public BinaryExpr {
public:
    LTExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return LT_EXPR; }
};

class GTExpr: public BinaryExpr {
public:
    GTExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return GT_EXPR; }
};

class LTEExpr: public BinaryExpr {
public:
    LTEExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return LTE_EXPR; }
};

class GTEExpr: public BinaryExpr {
public:
    GTEExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return GTE_EXPR; }
};

class NEExpr: public BinaryExpr {
public:
    NEExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return NE_EXPR; }
};

class EQExpr: public BinaryExpr {
public:
    EQExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return NE_EXPR; }
};

class AddExpr: public BinaryExpr {
public:
    AddExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return ADD_EXPR; }
};

class SubExpr: public BinaryExpr {
public:
    SubExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return SUB_EXPR; }
};

class MultExpr: public BinaryExpr {
public:
    MultExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return MULT_EXPR; }
};

class DivExpr: public BinaryExpr {
public:
    DivExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    
    VValue evaluate();
    string generateCode(string &place);
    int getKind() { return DIV_EXPR; }
};

class NumExpr: public Expr {
public:
    NumExpr(int value) { this->value = value; }
    VValue evaluate() { VValue v; v.type=DT_Int; v.ivalue = value; return v; }
    string generateCode(string &place);
    int getKind() { return NUM_EXPR; }
    
    int value;
};

class BoolExpr: public Expr {
public:
    BoolExpr(bool value) { this->value = value; }
    VValue evaluate() { VValue v; v.type=DT_Bool; v.ivalue = value; return v; }
    string generateCode(string &place);
    int getKind() { return BOOL_EXPR; }
    
    bool value;
};

class StringExpr: public Expr {
public:
    StringExpr(string value) { this->value = value; }
    VValue evaluate() { VValue v; return v; }
    string generateCode(string &place);
    int getKind() { return BOOL_EXPR; }
    
    string value;
};

class CharExpr: public Expr {
public:
    CharExpr(char value) { this->value = value; }
    VValue evaluate() { VValue v; v.type=DT_Int; v.ivalue = value; return v; }
    string generateCode(string &place);
    int getKind() { return CHAR_EXPR; }
    
    char value;
};

class IdExpr: public Expr {
public:
    IdExpr(string id) { this->id = id; }
    IdExpr(string id,Expr * arr_element_num) { this->id = id; this->arr_element_num = arr_element_num; }
    VValue evaluate() { return vars[id]; }
    string generateCode(string &place);
    int getKind() { return ID_EXPR; }
    
    string id;
    Expr * arr_element_num;
};

class MethodCallExpr:public Expr
{
public:
    MethodCallExpr(string id, ExprList * expr_list){
        this->id = id;
        this->expr_list = expr_list;
    }
    VValue evaluate() { return vars[id]; }
    string generateCode(string &place){}
    int getKind() { return METHOD_EXPR; }
    
    string id;
    ExprList * expr_list;
};


class IdElement{
public:
    bool isArray;
    int num_elements;
    string variable;
};

class IdElementVar:public IdElement{
public:
    IdElementVar(string var){
        variable = var;
    }
};

class IdElementArr:public IdElement{
public:
    IdElementArr(string var, int nums){
        variable = var;
        num_elements = nums;
        isArray = true;
    }
};

class Statement {
public:
    virtual void execute() = 0;
    virtual string generateCode() = 0;
    virtual StatementKind getKind() = 0;
    bool isA(int kind) { return (getKind() == kind); }
};

class VariablesDeclareStatement: public Statement {
public:
    VariablesDeclareStatement( DataType data_type, list<IdElement*> * idlist, Expr * defaultValue) { 
        this->idlist = idlist; 
        this->data_type = data_type;
        this->defaultValue = defaultValue;
    }
    void execute(){
        list<IdElement*>::iterator it = idlist->begin();
        VValue value = defaultValue->evaluate();


        while (it != idlist->end()) {
            IdElement *id_Expr = *it;

            value.type = data_type;

            vars[id_Expr->variable] = value;

            it++;
        };
    }
    string generateCode();
    StatementKind getKind() { return VARIABLE_DECLARE_STATEMENT; }
    
    list<IdElement*> * idlist;
    DataType data_type;
    Expr * defaultValue;
};

class BlockVariableStatement:public Statement
{
public:
    BlockVariableStatement( DataType data_type, list<string> * idlist) { 
        this->idlist = idlist; 
        this->data_type = data_type;
    }
    void execute(){}
    string generateCode();
    StatementKind getKind() { return VARIABLE_BLOCK_STATEMENT; }

    list<string> * idlist;
    DataType data_type;
};

class BlockStatement: public Statement {
public:
    BlockStatement(list<BlockVariableStatement *> * block_var,list<Statement *> * stList) { this->stList = stList; }
    void execute(){

    }
    string generateCode();
    StatementKind getKind() { return BLOCK_STATEMENT; }
    
    list<Statement *> * stList;
};

class ArgumentElement
{
public:
    ArgumentElement(DataType data_type, string arg_name){

    }
    
};

class AssigmentStament: public Statement{
public:
  AssigmentStament(IdExpr * assign_to, Expr * to_assign){
    this->assign_to = assign_to;
    this->to_assign = to_assign;
  }
  void execute() {}
  string generateCode();
  StatementKind getKind() { return ASSIGMENT_STATEMENT; }

  IdExpr * assign_to;
  Expr * to_assign;
};

class IfStatement: public Statement
{
public:
  IfStatement(Expr * eval, BlockStatement * then_block, BlockStatement * else_statement){
    this->eval = eval;
    this->then_block = then_block;
    this->else_statement = else_statement;
  }
  void execute() {}
  string generateCode();
  StatementKind getKind() { return IF_STATEMENT; }

  Expr * eval;
  BlockStatement * then_block;
  BlockStatement * else_statement
    
};

class MethodDeclareStatement: public Statement {
public: 
    MethodDeclareStatement(DataType data_type, string method_name,list<ArgumentElement *> * arguement_list, Statement * block_statement) {}
    void execute() {}
    string generateCode();
    StatementKind getKind() { return METHOD_DECLARE_STATEMENT; }
};

class MethodCallStatement:public Statement
{
public:
    MethodCallStatement(Expr * method_call){

    }
    void execute() {}
    string generateCode();
    StatementKind getKind() { return METHOD_CALL_STATEMENT; }
};

class ReturnStatement:public Statement
{
public:
    ReturnStatement(Expr * expr_one){

    }
    void execute() {}
    string generateCode();
    StatementKind getKind() { return RETURN_STATEMENT; }
};

class ContinueStatement:public Statement
{
public:
    ContinueStatement(){

    }
    void execute() {}
    string generateCode();
    StatementKind getKind() { return CONTINUE_STATEMENT; }
};

class BreakStatement:public Statement
{
public:
    BreakStatement(){

    }
    void execute() {}
    string generateCode();
    StatementKind getKind() { return BREAK_STATEMENT; }
};

class PrintStatement:public Statement
{
public:
    PrintStatement(ExprList * expr_list){

    }
    void execute() {}
    string generateCode();
    StatementKind getKind() { return PRINT_STATEMENT; }
};

class Class
{
public:
    Class(list<VariablesDeclareStatement*> * declare_list, list<MethodDeclareStatement *> * method_list){
        this->declare_list = declare_list;
        this->method_list = method_list;
    }

    void execute(){
        for (list <VariablesDeclareStatement *>::iterator it = declare_list -> begin(); it != declare_list -> end(); it++) {
            (*it)->execute();
        }

        for (list <MethodDeclareStatement *>::iterator it = method_list -> begin(); it != method_list -> end(); it++) {
            (*it)->execute();
        }
    }

    void generateCode();
    
    list<VariablesDeclareStatement*> * declare_list;
    list<MethodDeclareStatement *> * method_list;
};

#endif

