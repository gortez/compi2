%option noyywrap

%{
#include <cstdlib>
#include <cstring>
#include "ast.h"
#include "tokens.h"

int line = 0;

%}

DIGIT [0-9]
LETTER [a-zA-Z_]
CHARACTER [\x20-\x7e]

%x stringo

%%



"." { return '.'; }
"=" { return '='; }
"%" { return '%'; }
"!" { return '!'; }
"+" { return '+'; }
"-" { return '-'; }
"*" { return '*'; }
"/" { return '/'; }
"(" { return '('; }
")" { return ')'; }
";" { return ';'; }
"," { return ','; }
"{" { return '{'; }
"}" { return '}'; }
"[" { return '['; }
"]" { return ']'; }
"<" { return OP_LT; }
">" { return OP_GT; }
"<=" { return OP_LTE; }
">=" { return OP_GTE; }
"!=" { return OP_NE; }
"==" { return OP_EQ; }
[ \t] /* Nada */
[\n\r]  { line++; }
"&&" { return KW_AND; }
"||" { return KW_OR; }
"<<" { return KW_SHIFT_LEFT; }
">>" { return KW_SHIFT_RIGHT; }
"bool" { return KW_BOOL; }
"break" { return KW_BREAK; }
"print" { return KW_PRINT; }
"read" { return KW_READ; }
"continue" { return KW_CONTINUE; }
"class" { return KW_CLASS; }
"else" { return KW_ELSE; }
"extends" { return KW_EXTENDS; }
"false" { return KW_FALSE; }
"for" { return KW_FOR; }
"if" { return KW_IF; }
"int" { return KW_INT; }
"new" { return KW_NEW; }
"null" { return KW_NULL; }
"return" { return KW_RETURN; }
"rot" { return KW_ROT; }
"true" { return KW_TRUE; }
"void" { return KW_VOID; }
"real" { return KW_REAL; }
"while" { return KW_WHILE; }




{DIGIT}+ { yylval.num_t = atoi(yytext); return NUM; }
{LETTER}({DIGIT}|{LETTER})* { yylval.id_t = strdup(yytext); return ID; }
0x[0-9a-fA-F]+	{yylval.num_t = (int)strtol(yytext, NULL, 16); return NUM;}
'(\\.|[^\\'])+'  {yylval.num_t=yytext[0];return CHAR_L;}
"//"[^\n]*
\"(\\.|[^\\"])*\" {yylval.id_t=strdup(yytext);return STR_CONST;}
.   { printf("Simbolo no valido\n"); }

%%
