#include <cstdio>
#include "ast.h"

map<string, VValue> vars;

#define IMPLEMENT_RELATIONAL_EVALUATE(CLASS, OP)   \
VValue CLASS::evaluate()            \
{                                   \
		VValue value1 = expr1->evaluate();  \
		VValue value2 = expr2->evaluate();  \
		VValue result;                      \
																				\
		result.type = DT_Bool;               \
	result.ivalue = value1.ivalue OP value2.ivalue; \
				 \
		return result; \
}

#define IMPLEMENT_ARITHMETIC_EVALUATE(CLASS, OP)    \
VValue CLASS::evaluate()                \
{                                       \
		VValue value1 = expr1->evaluate();  \
		VValue value2 = expr2->evaluate();  \
		VValue result;                      \
																				\
		result.type = DT_Int;                                   \
		result.ivalue = value1.ivalue OP value2.ivalue;   \
									\
		return result;    \
}

IMPLEMENT_RELATIONAL_EVALUATE(OrExpr,||)
IMPLEMENT_RELATIONAL_EVALUATE(AndExpr,&&)
IMPLEMENT_RELATIONAL_EVALUATE(NEExpr,!=)
IMPLEMENT_RELATIONAL_EVALUATE(EQExpr,==)
IMPLEMENT_RELATIONAL_EVALUATE(LTExpr,<)
IMPLEMENT_RELATIONAL_EVALUATE(LTEExpr,<=)
IMPLEMENT_RELATIONAL_EVALUATE(GTExpr,>=)
IMPLEMENT_RELATIONAL_EVALUATE(GTEExpr,>)

IMPLEMENT_ARITHMETIC_EVALUATE(ShiftRightExpr,>>)
IMPLEMENT_ARITHMETIC_EVALUATE(ShiftLeftExpr,<<)
IMPLEMENT_ARITHMETIC_EVALUATE(AddExpr,+)
IMPLEMENT_ARITHMETIC_EVALUATE(ModExpr,%)
IMPLEMENT_ARITHMETIC_EVALUATE(SubExpr,-)
IMPLEMENT_ARITHMETIC_EVALUATE(DivExpr,/)
IMPLEMENT_ARITHMETIC_EVALUATE(MultExpr,*)

const char *temps[] = {"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8", "$t9"};
#define TEMP_COUNT (sizeof(temps)/sizeof(temps[0]))
set<string> var_set;
map<string,string> string_set;


int str_counter = 0;
int lbl_counter = 0;

set<string> tempRegSet;

string newTemp()
{
		int i;
		
		for (i=0; i<TEMP_COUNT; i++) {
				if (tempRegSet.find(temps[i]) == tempRegSet.end()) {
						tempRegSet.insert(temps[i]);
						
						return string(temps[i]);
				}
		}
		
		cout << "temp overflow!"<< endl;
		exit(0);

		return string("");
}


string newLabel() {
    stringstream sstream;
    sstream << "LBL" << lbl_counter++;
    return sstream.str();
}

void releaseTemp(string temp)
{
		tempRegSet.erase(temp);
}

string NegExpr::generateCode(string &place)
{

	return string("");
}

string RotExpr::generateCode(string &place)
{

	return string("");
}

string ShiftRightExpr::generateCode(string &place)
{

	return string("");
}

string ShiftLeftExpr::generateCode(string &place)
{

	return string("");
}

string OrExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl
			 << code2 << endl 
			 << "or " << place << ", " << place1 << ", " << place2 << endl;
		
		return ss.str();
}

string AndExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl
			 << code2 << endl
			 << "and " << place << ", " << place1 << ", " << place2 << endl;
		
		return ss.str();
}

string LTExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl 
			 << code2 << endl 
			 << "slt " << place << ", " << place1 << ", " << place2 << endl;
		
		return ss.str();
}

string GTExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl
			 << code2 << endl
			 << "slt " << place << ", " << place2 << ", " << place1 << endl;
		
		return ss.str();
}

string LTEExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl
			 << code2 << endl
			 << "sle " << place << ", " << place1 << ", " << place2 << endl;
		
		return ss.str();
}

string GTEExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl
			 << code2 << endl
			 << "sge " << place << ", " << place1 << ", " << place2 << endl;
		
		return ss.str();
}

string EQExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl
			 << code2 << endl
			 << "seq " << place << ", " << place1 << ", " << place2 << endl;
		
		return ss.str();
}

string NEExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl 
			 << code2 << endl 
			 << "sne " << place << ", " << place1 << ", " << place2 << endl;
		
		return ss.str();
}

string AddExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl 
			 << code2 << endl 
			 << "add " << place << ", " << place1 << ", " << place2 << endl;
					
		return ss.str();
}

string SubExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl
			 << code2 << endl 
			 << "sub " << place << ", " << place1 << ", " << place2 << endl;
		
		return ss.str();
}

string MultExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl <<
		code2 << endl <<
		"mult " << place1 << ", " << place2 << endl <<
		"mflo " << place << endl;
		
		return ss.str();
}

string DivExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl
			 << code2 << endl
			 << "div " << place1 << ", " << place2 << endl 
			 << "mflo " << place << endl;
		
		return ss.str();
}

string ModExpr::generateCode(string &place)
{
		string place1, place2;
		string code1 = expr1->generateCode(place1);
		string code2 = expr2->generateCode(place2);
		stringstream ss;
		
		releaseTemp(place1);
		releaseTemp(place2);
		place = newTemp();
		
		ss << code1 << endl
			 << code2 << endl
			 << "div " << place1 << ", " << place2 << endl 
			 << "mfhi " << place;
		
		return ss.str();
}

string NumExpr::generateCode(string &place)
{
		stringstream ss;
		
		place = newTemp();
		
		ss << "li " << place << ", " << value;
		
		return ss.str();
}

string IdExpr::generateCode(string &place)
{
		if (vars.find(id) == vars.end()) {
        printf("Id %s no existe!\n", id.c_str());
        exit(0);
    }
		stringstream ss;
		
		place = newTemp();
		
		ss << "la " << place << ", " << id << endl
			 << "lw " << place << ", 0(" << place << ")";
		

		//TODO:Arreglos
		return ss.str();
}

string StringExpr::generateCode(string &place)
{
		string label = "STR_";
		label += to_string(str_counter++);
		string_set[label] = str;
		place = label;

		return string("");
}

string BlockStatement::generateCode()
{
		list<Statement *>::iterator it = stList.begin();
		stringstream ss;
		
		while (it != stList.end()) {
				Statement *st = *it;
				
				ss << st->generateCode() << endl;
				it++;
		}   
		
		return ss.str();
}

string WritelnStatement::generateCode() 
{
		ExprList::iterator it = lexpr.begin();
		stringstream ss;
		
		while (it != lexpr.end()) {
				Expr *st = *it;
				
				string place1;
				string code1 = st->generateCode(place1);
				
				if(st->getKind() == STRING_EXPR){
						ss << code1 << endl
							 << "la $a0, " << place1 << endl
							 << "li $v0, 4\n"
							 << "syscall\n"; 
				}else{
						ss << code1 << endl
							 << "move $a0, " << place1 << endl
							 << "li $v0, 1\n"
							 << "syscall\n"; 

						releaseTemp(place1);
				}
				
				it++;
		}  

		return ss.str();
}

string AssigmentStament::generateCode()
{
		string code, codeId, placeExpr, placeId, temp;
		stringstream ss;
		
		code = to_assign->generateCode(placeExpr);
		codeId = assign_to-generateCode(placeId);
		temp = newTemp();
		
		ss << code << endl
			 << "la " << temp << ", " << id << endl
			 << codeId << endl
			 << "sw " << placeId << ", 0(" << temp << ")";
			 
		releaseTemp(placeId);
		releaseTemp(temp);
		
		return ss.str();    
}

string IfStatement::generateCode(string start, string end) 
{
		string placeExpr, then_code, else_code;
		stringstream sstream;

    string exprCode = eval->generateCode(placeExpr);

    string elseLabel = newLabel();
    string endLabel = newLabel();
    
    releaseTemp(placeExpr);

    string then_code = then_block->generateCode(start, end);
    if (else_statement)
        else_code = else_statement->generateCode(start, end);

    sstream << exprCode << endl 
       			<< "beqz " << placeExpr << ", " << elseLabel << endl 
       			<< then_code << endl
       			<< "j " << endLabel << endl
       			<< elseLabel << ": "<< endl 
       			<< else_code << endl
       			<< endLabel << ": " << endl

    return sstream.str();
}

string WhileStatement::generateCode()
{
		return string("");
}
