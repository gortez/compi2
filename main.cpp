#include <cstdio>
#include "ast.h"
#include "tokens.h"

extern Class *input;
extern set<string> var_set;
extern map<string,string> string_set;

int main()
{
    input = 0;
    yyparse();
    
    if (input != 0) {
        cout << "\t.data" << endl;

        string code = input->generateCode();

        set<string>::iterator it = var_set.begin();

		while (it != var_set.end()) {
		    string st = *it;
		    
		    cout << st << ": .word 0" << endl;
		    it++;
		}

		map<string,string>::iterator it2 = string_set.begin();

		while (it2 != string_set.end()) {
		    //string st = *it2;
		    
		    cout << it2->first << ": .asciiz \""<< it2->second << "\" " << endl;
		    it2++;
		}
        
        cout << "\t.text" << endl;

        cout << code << endl;
        
        cout << "li $v0, 10 " << endl
             << "syscall" << endl;
    }
}
