%{
#include <cstdio>
#include <cstdlib>
#include <string>
#include "ast.h"

using namespace std;

extern int line;

int yylex();

void yyerror(const char *str)
{
    printf("Line %d: %s\n", line, str);
    exit(0);
}

#define YYERROR_VERBOSE 1

Class *input;

%}

%union {
    char *id_t;
    int  num_t;
    Statement *statement_t;
    list<Statement*> * statement_list_t;
    ExprList *exprlist_t;
    Expr *expr_single;
    IdExpr * id_expr_t;
    IdDeclare *id_declare_t;
    VariablesDeclareStatement * var_declar_single_t;
    list<VariablesDeclareStatement*> * list_var_declr_t;
    list<MethodDeclareStatement *> * list_method_declr_t;
    MethodDeclareStatement * method_single_t;
    DataType data_type_t;
    BlockVariableStatement * block_var_single_t;
    list<IdElement*> * id_list_t;
    list<ArgumentElement *> * arg_list_t;
    list<BlockVariableStatement *> * local_list_declr_t;
    list<string> * str_list_t;

}

%token<num_t> NUM
%token<id_t> ID
%token<id_t> STR_CONST
%token <num_t> CHAR_L
%token PRINT INTEGER DOUBLE
%token OP_LT OP_GT OP_LTE OP_GTE OP_NE OP_EQ
%token KW_AND KW_OR KW_SHIFT_LEFT KW_SHIFT_RIGHT
%token KW_BOOL KW_BREAK KW_PRINT KW_READ
%token KW_CONTINUE KW_CLASS KW_ELSE KW_EXTENDS
%token KW_FALSE KW_FOR KW_IF KW_INT KW_REAL
%token KW_NEW KW_NULL KW_RETURN KW_ROT
%token KW_TRUE KW_VOID KW_WHILE KW_CONST_STRING

%type<exprlist_t> methodParameter parameter
%type<id_expr_t> arr_or_id
%type<expr_single> expr return_expr constant MethodExpr
%type<statement_t> statement_single  else_block
%type<list_var_declr_t> global_declare_various
%type<list_method_declr_t> method_various
%type<statement_t>  block 
%type<block_var_single_t> block_var_individual
%type<var_declar_single_t> global_declare_one
%type<data_type_t> type
%type<id_list_t> id_list id_individual
%type<arg_list_t> ListaArgumentos ArgumentProd
%type<local_list_declr_t> block_var_various
%type<str_list_t> variableList
%type<method_single_t> method_individual
%type<statement_list_t> block_statements

%left KW_OR
%left KW_AND
%left OP_NE OP_EQ
%left OP_LT OP_LTE OP_GTE OP_GT
%left KW_SHIFT_LEFT KW_SHIFT_RIGHT KW_ROT
%left '%'
%left '+' '-'
%left '*' '/'
%right '!'
%left U_MINUS

%%

input:  KW_CLASS ID '{'
        global_declare_various method_various
        '}'
        { input = new Class($4,$5); }
;

global_declare_various:                                               { $$ = new list<VariablesDeclareStatement*>();}
                      | global_declare_various global_declare_one ';' { $$ = $1;
                                                                        $$->push_back($2);
                                                                      }

global_declare_one: type id_list             { 
                                                 $$ = new VariablesDeclareStatement($1,$2,new NumExpr(0));
                                             }
                  | type ID '=' constant    {
                                                list<IdElement*> * _list = new list<IdElement*>();
                                                IdElementVar *id_element=new IdElementVar($2);
                                                _list->push_back(id_element);
                                                $$ = new VariablesDeclareStatement($1, _list, $4);
                                             }

id_list: ID id_individual             {
                                        IdElementVar *id_element=new IdElementVar($1);
                                        $2->push_front(id_element);
                                        $$=$2;
                                      }
       | ID '[' NUM ']' id_individual {
                                        IdElementArr *id_element=new IdElementArr($1,$3);
                                        $5->push_front(id_element);
                                        $$=$5;
                                      }

id_individual:',' ID id_individual              {
                                                    $$ =$3;
                                                    IdElementVar *id_element=new IdElementVar($2);
                                                    $$->push_front(id_element); 
                                                }
             |',' ID '[' NUM ']' id_individual  {
                                                    $$=$6;
                                                    IdElementArr *id_element=new IdElementArr($2,$4);
                                                    $$->push_front(id_element);
                                                }
             |                                  {   $$ = new list<IdElement*>();    }                                            

method_various:                                 {
                                                    $$ = new list<MethodDeclareStatement *>();
                                                }
              |method_individual method_various {
                                                    $$=$2; 
                                                    $$->push_front($1);
                                                }

method_individual: type ID ListaArgumentos block {
                                                    $$ = new MethodDeclareStatement($1,$2,$3,$4);
                                                 }

ListaArgumentos: '(' ')'          {
                                    $$ = new list<ArgumentElement *> ();
                                  }
            | '(' type ID ArgumentProd ')' {
                                    ArgumentElement *arg_element= new ArgumentElement($2,$3);
                                    $$=$4;
                                    $$->push_front(arg_element); 
                                  }


ArgumentProd:                   {
                                    $$ = new list<ArgumentElement *>(); 
                                }
            |',' type ID ArgumentProd    {
                                    $$=$4;
                                    ArgumentElement *arg_element = new ArgumentElement($2,$3);
                                    $$->push_front(arg_element);
                                }

type: KW_BOOL {
                $$ = DT_Bool;
              }
    | KW_INT {
                $$ = DT_Int;
             }
    | KW_REAL {
                $$ = DT_Real;
             }
    | KW_VOID {
                $$ = DT_Void;
             }

block: '{' block_var_various block_statements '}'{ $$=new BlockStatement($2,$3); }

block_var_various:                                            {$$ = new list<BlockVariableStatement *>();}
                 |block_var_individual ';' block_var_various  {
                                                                $$ = $3;
                                                                $3->push_front($1);
                                                              };

block_var_individual: type ID variableList {
                                                $3->push_back($2);
                                                $$= new BlockVariableStatement($1,$3);
                                           };

variableList:                    {$$ = new list<string>();}
            |',' ID variableList {$$=$3; $$->push_front($2); };

block_statements:                                  {$$ = new list<Statement *>();}
                |block_statements statement_single {$$ = $1;$1->push_back($2);};


statement_single:arr_or_id '=' expr ';' {$$ = new AssigmentStament($1,$3);} 
                |MethodExpr ';' {$$ = new MethodCallStatement($1); }
                |KW_BREAK ';' {$$ = new BreakStatement();}
                |KW_RETURN return_expr ';' {$$ = new ReturnStatement($2);}
                |KW_CONTINUE ';' {$$ = new ContinueStatement();}
                |KW_IF '(' expr ')' block else_block {$$ = new IfStatement($3,$5,$6);}
                |KW_PRINT methodParameter ';'	{$$ = new PrintStatement($2); }
                |block {$$ = $1;};
            
methodParameter:              {$$ = new list<Expr*>();}
             | expr parameter {$$=$2;$$->push_front($1);}

parameter:                   {$$ = new list<Expr*>();}
         |',' expr parameter {
                              $$=$3; 
                              $$->push_front($2);
                             };


else_block:             {$$ = new BlockStatement(new list<BlockVariableStatement *>(), new list<Statement *>());}
            |KW_ELSE block {$$ = $2;}


return_expr:        {$$ = new NumExpr(0);}
            |expr   {$$ = $1;}

constant: NUM       {  $$ = new NumExpr($1); }
        | KW_TRUE   {  $$ = new BoolExpr(true); }
        | KW_FALSE  {  $$ = new BoolExpr(false); }
        | CHAR_L    {  $$ = new CharExpr($1); }

expr: expr KW_SHIFT_RIGHT expr      { $$ = new ShiftRightExpr($1,$3); }
    | expr KW_SHIFT_LEFT expr       { $$ = new ShiftLeftExpr($1,$3); }
    | expr KW_OR expr               { $$ = new OrExpr($1,$3); }
    | expr KW_AND expr              { $$ = new AndExpr($1,$3); }
    | expr OP_NE expr               { $$ = new NEExpr($1,$3); }
    | expr OP_EQ expr               { $$ = new EQExpr($1,$3); }
    | expr OP_LT expr               { $$ = new LTExpr($1,$3); }
    | expr OP_LTE expr              { $$ = new LTEExpr($1,$3); }
    | expr OP_GTE expr              { $$ = new GTEExpr($1,$3); }
    | expr OP_GT expr               { $$ = new GTExpr($1,$3); }
    | expr KW_ROT expr              { $$ = new RotExpr($1,$3); }
    | expr '%' expr                 { $$ = new ModExpr($1,$3); }
    | expr '-' expr                 { $$ = new SubExpr($1,$3); }
    | expr '+' expr                 { $$ = new AddExpr($1,$3); }
    | expr '/' expr                 { $$ = new DivExpr($1,$3); }
    | expr '*' expr                 { $$ = new MultExpr($1,$3); }
    | '!' expr                      { $$ = new NegExpr($2);}
    | '(' expr ')'                  { $$ = $2; }
    | '-' expr %prec U_MINUS        { $$ = new MultExpr(new NumExpr(-1), $2); }
    | NUM                           { $$ = new NumExpr($1); }
    | STR_CONST                     { $$ = new StringExpr($1); }
    | arr_or_id                     { $$ = $1; }
    | MethodExpr 										{ $$ = $1; }

MethodExpr: ID '('methodParameter')' {$$ = new MethodCallExpr($1,$3); }      

arr_or_id: ID               { 
                                string id = $1;
                                
                                free($1);
                                $$ = new IdExpr(id);
                            }
         | ID '[' expr ']'  {$$ = new IdExpr($1,$3);};

%%