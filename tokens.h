/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_TOKENS_H_INCLUDED
# define YY_YY_TOKENS_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    NUM = 258,
    ID = 259,
    STR_CONST = 260,
    CHAR_L = 261,
    PRINT = 262,
    INTEGER = 263,
    DOUBLE = 264,
    OP_LT = 265,
    OP_GT = 266,
    OP_LTE = 267,
    OP_GTE = 268,
    OP_NE = 269,
    OP_EQ = 270,
    KW_AND = 271,
    KW_OR = 272,
    KW_SHIFT_LEFT = 273,
    KW_SHIFT_RIGHT = 274,
    KW_BOOL = 275,
    KW_BREAK = 276,
    KW_PRINT = 277,
    KW_READ = 278,
    KW_CONTINUE = 279,
    KW_CLASS = 280,
    KW_ELSE = 281,
    KW_EXTENDS = 282,
    KW_FALSE = 283,
    KW_FOR = 284,
    KW_IF = 285,
    KW_INT = 286,
    KW_REAL = 287,
    KW_NEW = 288,
    KW_NULL = 289,
    KW_RETURN = 290,
    KW_ROT = 291,
    KW_TRUE = 292,
    KW_VOID = 293,
    KW_WHILE = 294,
    KW_CONST_STRING = 295,
    U_MINUS = 296
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 25 "tinyc.y" /* yacc.c:1915  */

    char *id_t;
    int  num_t;
    Statement *statement_t;
    list<Statement*> * statement_list_t;
    ExprList *exprlist_t;
    Expr *expr_single;
    IdExpr * id_expr_t;
    IdDeclare *id_declare_t;
    VariablesDeclareStatement * var_declar_single_t;
    list<VariablesDeclareStatement*> * list_var_declr_t;
    list<MethodDeclareStatement *> * list_method_declr_t;
    MethodDeclareStatement * method_single_t;
    DataType data_type_t;
    BlockVariableStatement * block_var_single_t;
    list<IdElement*> * id_list_t;
    list<ArgumentElement *> * arg_list_t;
    list<BlockVariableStatement *> * local_list_declr_t;
    list<string> * str_list_t;


#line 118 "tokens.h" /* yacc.c:1915  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_TOKENS_H_INCLUDED  */
